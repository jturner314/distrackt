# Distrackt

Distrackt is a program designed to remove tracking from URLs, such as those in
marketing emails.

## Usage

Pass the original URL to standard input, and the sanitized URL will be printed
to standard output.

```shell
$ echo -n 'https://www.example.com/?utm_source=foo' | distrackt
https://www.example.com/?
```

## Supported Trackers

Currently, distrackt has support for removing the following trackers:

* [Eloqua](https://www.oracle.com/marketingcloud/products/marketing-automation/index.html)
* [Google Analytics](https://www.google.com/analytics/)
* [iContact](https://www.icontact.com/)
* [Proofpoint TAP](https://www.proofpoint.com/us/solutions/products/targeted-attack-protection)
* SubID

## Installation

Distrackt is written in [Rust](https://www.rust-lang.org/en-US/).

### Dependencies

* [`cargo`](https://crates.io/install) must be installed and on your `PATH`.
* `gzip` must be installed and on your `PATH`.

### Building and Installing

To build the program and install it, run:

```shell
make
sudo make install
```

### Integrating with mu4e

If you use the [mu4e][] mail client and you'd like to automatically call
`distrackt` on URLs when opening them with `mu4e-view-go-to-url` or copying
them with `mu4e-view-save-url`, add the following to your `.emacs`:

```elisp
(require 'mu4e)

(defvar distrackt-command
  "distrackt"
  "Path to `distrackt' program which removes trackers from URLs.")

(defun distrackt-url (url)
  "Remove tracking on a URL."
  (with-temp-buffer
    (insert url)
    (with-output-to-string
      (shell-command-on-region
       (point-min) (point-max) distrackt-command standard-output))))

(defun mu4e-view-go-to-url (&optional multi)
  "Offer to go to url(s). If MULTI (prefix-argument) is nil, go to
a single one, otherwise, offer to go to a range of urls."
  (interactive "P")
  (mu4e~view-handle-urls
   "URL to visit" multi
   (lambda (url)
     (let ((fixed-url (distrackt-url url)))
       (mu4e~view-browse-url-from-binding fixed-url)))))

(defun mu4e-view-save-url (&optional multi)
  "Offer to save urls(s) to the kill-ring. If
MULTI (prefix-argument) is nil, save a single one, otherwise, offer
to save a range of URLs."
  (interactive "P")
  (mu4e~view-handle-urls
   "URL to save" multi
   (lambda (url)
     (let ((fixed-url (distrackt-url url)))
       (kill-new fixed-url)
       (mu4e-message "Saved %s to the kill-ring" fixed-url)))))
```

[mu4e]: http://www.djcbsoftware.nl/code/mu/mu4e.html

## License

Distrackt is copyright 2016, Jim Turner.

Licensed under the Apache License (Version 2.0) or the MIT license, at your
option. See [LICENSE-APACHE](LICENSE-APACHE), [LICENSE-MIT](LICENSE-MIT), and
[LICENSE](LICENSE) for details.

## Contributing

This is my first public Rust library and program, and I'm open to suggestions
to improve the program. Please create issues if you find any bugs or send pull
requests with any improvements.
