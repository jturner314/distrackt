// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

extern crate distrackt;
extern crate url;

use distrackt::url_filters::{self, UrlFilter};
use std::io::{self, Read};
use url::Url;

fn main() {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer).unwrap();
    let original = Url::parse(&buffer).unwrap();

    let filters = url_filters::all_filters();

    let new = filters.apply(&original).unwrap();

    print!("{}", new.as_str());
}
