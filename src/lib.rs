// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! `distrackt` is a library to remove tracking, primarily for mangled URLs in
//! marketing emails.

extern crate url;

pub mod url_filters;
