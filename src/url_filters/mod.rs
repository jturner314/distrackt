// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! Filters to remove tracking from URLs.
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::{all_filters, UrlFilter};
//!     use url::Url;
//!
//!     let example = "https://urldefense.proofpoint.com/v2/url?u=http-3A__click.icptrack.com_icp_relay.php-3Fr-3D46539473-26msgid-3D36e526-26act-3DC9FA-26c-3D3539846-26destination-3Dhttps-253A-252F-252Fwww.example.com-252F&d=UcEOnW&c=beOCMzW38NgfcGTEVpciebkfEBsCj836COgj_wD7cif&r=7CNBEIdnduendDFDNEIdbKFDNEKFDMFYLODNMurnmco&m=hjceic7ekd-hduckeIDMCIefiudKDIEMCKG-iemcOIFMCIDHJmdicmeicjusPORUCM-KDIEMCkduecIEMCImcidmc&e=";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = all_filters();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "https://www.example.com/");
//! }
//! ```

use std::error::Error;
use std::fmt;
use std::str;
use url::{self, Url};

pub mod eloqua;
pub mod google_analytics;
pub mod icontact;
pub mod proofpoint;
pub mod subid;

mod util;

/// A filter to check for and remove tracking from URLs.
pub trait UrlFilter {
    /// Checks if the filter applies to the URL.
    fn check(&self, original: &Url) -> bool;

    /// Applies the filter to the URL.
    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError>;
}

/// A set of multiple `UrlFilter`s to be used as one.
///
/// Note that the `UrlFilter` trait is implemented for `UrlFilterSet`.
pub type UrlFilterSet = Vec<Box<UrlFilter>>;

/// Returns a `UrlFilterSet` containing all available filters.
pub fn all_filters() -> UrlFilterSet {
    vec![
        Box::new(proofpoint::Proofpoint::new()),
        Box::new(icontact::IContact::new()),
        Box::new(google_analytics::GoogleAnalytics::new()),
        Box::new(eloqua::Eloqua::new()),
        Box::new(subid::SubId::new()),
    ]
}

impl UrlFilter for UrlFilterSet {
    fn check(&self, original: &Url) -> bool {
        self.iter().any(|filter| filter.check(&original))
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        let mut new = original.clone();
        loop {
            let mut changed = false;
            for filter in self {
                if filter.check(&new) {
                    let url = try!(filter.apply(&new));
                    if url != new {
                        new = url;
                        changed = true;
                    }
                }
            }
            if !changed {
                break;
            }
        }
        Ok(new)
    }
}

/// An error that represents a missing query pair in a URL.
///
/// Some URL mangling schemes (including those by Proofpoint and iContact)
/// include the destination URL as a query value. If the query pair supposed to
/// contain the destination URL is missing, the destination URL cannot be
/// found.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct MissingQueryError {
    key: String,
}

impl MissingQueryError {
    /// Constructs a new `MissingQueryError` with the specified query key.
    ///
    /// # Examples
    ///
    /// ```
    /// use distrackt::url_filters::MissingQueryError;
    ///
    /// let error = MissingQueryError::new("destination");
    /// ```
    pub fn new(key: &str) -> MissingQueryError {
        MissingQueryError {
            key: key.to_string()
        }
    }
}

impl fmt::Display for MissingQueryError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "missing query pair with key: {}", self.key)
    }
}

impl Error for MissingQueryError {
    fn description(&self) -> &str {
        "missing query pair with desired key"
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

/// An error that represents a failure to apply a URL filter.
#[derive(Clone,Debug,PartialEq)]
pub enum UrlFilterError {
    UrlParse(url::ParseError),
    Utf8(str::Utf8Error),
    MissingQuery(MissingQueryError),
}

impl fmt::Display for UrlFilterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            UrlFilterError::UrlParse(ref err) => write!(f, "URL parse error: {}", err),
            UrlFilterError::Utf8(ref err) => write!(f, "UTF8 not well-formed error: {}", err),
            UrlFilterError::MissingQuery(ref err) => write!(f, "missing query error: {}", err),
        }
    }
}

impl Error for UrlFilterError {
    fn description(&self) -> &str {
        match *self {
            UrlFilterError::UrlParse(ref err) => err.description(),
            UrlFilterError::Utf8(ref err) => err.description(),
            UrlFilterError::MissingQuery(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            UrlFilterError::UrlParse(ref err) => Some(err),
            UrlFilterError::Utf8(ref err) => Some(err),
            UrlFilterError::MissingQuery(ref err) => Some(err),
        }
    }
}

impl From<url::ParseError> for UrlFilterError {
    fn from(err: url::ParseError) -> UrlFilterError {
        UrlFilterError::UrlParse(err)
    }
}

impl From<str::Utf8Error> for UrlFilterError {
    fn from(err: str::Utf8Error) -> UrlFilterError {
        UrlFilterError::Utf8(err)
    }
}

impl From<MissingQueryError> for UrlFilterError {
    fn from(err: MissingQueryError) -> UrlFilterError {
        UrlFilterError::MissingQuery(err)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use url::Url;

    #[test]
    fn all_filters_not_applicable() {
        let filter = all_filters();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://www.google.com/");
    }

    #[test]
    fn google_analytics_and_eloqua_and_subid() {
        let filter: UrlFilterSet = vec![
            Box::new(google_analytics::GoogleAnalytics::new()),
            Box::new(eloqua::Eloqua::new()),
            Box::new(subid::SubId::new()),
        ];

        let url = Url::parse("http://www.mouser.com/?utm_medium=email&utm_camp\
aign=elq-73.7442-eit-iss-brand-en&utm_source=eloqua&subid=f82abc7394d263eca845\
ee733ecf594a&utm_content=7639463&elqTrackId=0123456789abcdef73c2e1907d61ec3b&e\
lq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&elqat=1&elqCampaignId=6538").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "http://www.mouser.com/?");
    }

    #[test]
    fn proofpoint_and_icontact() {
        let filter: UrlFilterSet = vec![
            Box::new(proofpoint::Proofpoint::new()),
            Box::new(icontact::IContact::new()),
        ];

        let url = Url::parse("https://urldefense.proofpoint.com/v2/url?u=http-\
3A__click.icptrack.com_icp_relay.php-3Fr-3D46539473-26msgid-3D36e526-26act-3DC\
9FA-26c-3D3539846-26destination-3Dhttps-253A-252F-252Fcolab.duke.edu-252Froots\
&d=UcEOnW&c=beOCMzW38NgfcGTEVpciebkfEBsCj836COgj_wD7cif&r=7CNBEIdnduendDFDNEId\
bKFDNEKFDMFYLODNMurnmco&m=hjceic7ekd-hduckeIDMCIefiudKDIEMCKG-iemcOIFMCIDHJmdi\
cmeicjusPORUCM-KDIEMCkduecIEMCImcidmc&e=").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://colab.duke.edu/roots");
    }

}
