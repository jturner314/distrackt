// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

use std::borrow::Cow;
use url::Url;
use super::MissingQueryError;

pub fn query_pair_value<'a>(url: &'a Url, key: &str) -> Result<Cow<'a, str>, MissingQueryError> {
    let (_, value) = try!(url.query_pairs()
        .find(|&(ref k, _)| k == key)
        .ok_or(MissingQueryError::new(key)));
    Ok(value)
}

pub fn query_pairs_remove(original: &Url, keys: &[&str]) -> Url {
    let mut new = original.clone();
    original.query_pairs()
        .fold(new.query_pairs_mut().clear(), |new_pairs, (key, value)| {
            if !keys.contains(&&(*key)) {
                new_pairs.append_pair(&key, &value)
            } else {
                new_pairs
            }
        });
    new
}
