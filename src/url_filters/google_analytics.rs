// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! A filter to remove Google Analytics tracking.
//!
//! [Google Analytics](https://www.google.com/analytics/) tracking is performed
//! by adding query parameters with any of the keys listed in `UTM_KEYS`. This
//! filter removes all query pairs with any of those keys.
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::google_analytics::GoogleAnalytics;
//!     use distrackt::url_filters::UrlFilter;
//!     use url::Url;
//!
//!     let example = "http://www.example.com/?utm_medium=email&utm_campaign=elq-73.7442-eit-iss-brand-en&utm_source=eloqua&utm_content=7639463";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = GoogleAnalytics::new();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "http://www.example.com/?");
//! }
//! ```

use url::Url;
use super::{util, UrlFilter, UrlFilterError};

/// A filter to remove Google Analytics tracking.
#[derive(Clone)]
pub struct GoogleAnalytics;

/// The query keys used for Google Analytics tracking.
///
/// See the [Google Analytics documentation]
/// (https://support.google.com/analytics/answer/1033867)
/// for more information about the various tracking keys.
pub const UTM_KEYS: &'static [&'static str] =
    &["utm_medium", "utm_campaign", "utm_content", "utm_source", "utm_term"];

impl GoogleAnalytics {
    /// Constructs a new `GoogleAnalytics`.
    pub fn new() -> GoogleAnalytics {
        GoogleAnalytics {}
    }
}

impl UrlFilter for GoogleAnalytics {
    fn check(&self, original: &Url) -> bool {
        original.query_pairs().any(|(key, _)| UTM_KEYS.contains(&&(*key)))
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        Ok(util::query_pairs_remove(&original, UTM_KEYS))
    }
}

#[cfg(test)]
mod tests {
    use super::GoogleAnalytics;
    use super::super::UrlFilter;
    use url::Url;

    #[test]
    fn good() {
        let filter = GoogleAnalytics::new();

        let url = Url::parse("http://www.mouser.com/?utm_medium=email&utm_camp\
aign=elq-73.7442-eit-iss-brand-en&utm_source=eloqua&subid=f82abc7394d263eca845\
ee733ecf594a&utm_content=7639463&elqTrackId=0123456789abcdef73c2e1907d61ec3b&e\
lq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&elqat=1&elqCampaignId=6538").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "http://www.mouser.com/?subid=f82abc7394d263eca845ee733ecf5\
94a&elqTrackId=0123456789abcdef73c2e1907d61ec3b&elq=f82abc7394d263eca845ee733e\
cf594a&elqaid=7352&elqat=1&elqCampaignId=6538");
    }

    #[test]
    fn not_applicable() {
        let filter = GoogleAnalytics::new();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://www.google.com/?");
    }
}
