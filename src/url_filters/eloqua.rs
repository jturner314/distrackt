// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! A filter to remove Eloqua tracking.
//!
//! [Eloqua][] tracking is performed by adding query parameters with any of the
//! keys listed in `ELQ_KEYS`. This filter removes all query pairs with any of
//! those keys.
//!
//! [Eloqua]: https://www.oracle.com/marketingcloud/products/marketing-automation/index.html
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::eloqua::Eloqua;
//!     use distrackt::url_filters::UrlFilter;
//!     use url::Url;
//!
//!     let example = "http://www.example.com/?elqTrackId=0123456789abcdef73c2e1907d61ec3b&elq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&elqat=1&elqCampaignId=6538";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = Eloqua::new();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "http://www.example.com/?");
//! }
//! ```

use url::Url;
use super::{util, UrlFilter, UrlFilterError};

#[derive(Clone)]
pub struct Eloqua;

/// The query keys used for Eloqua tracking.
///
/// See the [Eloqua documentation]
/// (http://docs.oracle.com/cloud/latest/marketingcs_gs/OMCAA/index.html#Help/General/EloquaTrackingParameters.htm)
/// for more information about the various tracking keys.
pub const ELQ_KEYS: &'static [&'static str] = &["elqTrackId",
                                                "elqTrack",
                                                "elqAssetType",
                                                "elqAssetId",
                                                "elqRecipientId",
                                                "elqCampaignId",
                                                "elqSiteId",
                                                "elq",
                                                "elqat",
                                                "elqaid",
                                                "elq_mid"];

impl Eloqua {
    /// Constructs a new `Eloqua`.
    pub fn new() -> Eloqua {
        Eloqua {}
    }
}

impl UrlFilter for Eloqua {
    fn check(&self, original: &Url) -> bool {
        original.query_pairs().any(|(key, _)| ELQ_KEYS.contains(&&(*key)))
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        Ok(util::query_pairs_remove(&original, ELQ_KEYS))
    }
}

#[cfg(test)]
mod tests {
    use super::Eloqua;
    use super::super::UrlFilter;
    use url::Url;

    #[test]
    fn good() {
        let filter = Eloqua::new();

        let url = Url::parse("http://www.mouser.com/?utm_medium=email&utm_camp\
aign=elq-73.7442-eit-iss-brand-en&utm_source=eloqua&subid=f82abc7394d263eca845\
ee733ecf594a&utm_content=7639463&elqTrackId=0123456789abcdef73c2e1907d61ec3b&e\
lq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&elqat=1&elqCampaignId=6538").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "http://www.mouser.com/?utm_medium=email&utm_campaign=elq-7\
3.7442-eit-iss-brand-en&utm_source=eloqua&subid=f82abc7394d263eca845ee733ecf59\
4a&utm_content=7639463");
    }

    #[test]
    fn not_applicable() {
        let filter = Eloqua::new();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://www.google.com/?");
    }
}
