// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! A filter to remove iContact tracking.
//!
//! [iContact](https://www.icontact.com/) tracking is performed by modifying
//! the URL to point to iContact's servers and including the destination URL in
//! the query string. This filter extracts the true destination URL from
//! the query string.
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::icontact::IContact;
//!     use distrackt::url_filters::UrlFilter;
//!     use url::Url;
//!
//!     let example = "http://click.icptrack.com/icpf/relay.php?r=83759739&msgid=47283&act=8SSS&c=3748204&destination=https%3A%2F%2Fwww.example.com%2F";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = IContact::new();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "https://www.example.com/");
//! }
//! ```

use url::Url;
use super::{util, UrlFilter, UrlFilterError};

/// A filter to remove iContact tracking.
#[derive(Clone)]
pub struct IContact;

impl IContact {
    /// Constructs a new `IContact`.
    pub fn new() -> IContact {
        IContact {}
    }
}

impl UrlFilter for IContact {
    fn check(&self, original: &Url) -> bool {
        original.domain() == Some("click.icptrack.com")
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        let mangled = try!(util::query_pair_value(&original, "destination"));
        Ok(try!(Url::parse(&mangled)))
    }
}

#[cfg(test)]
mod tests {
    use super::IContact;
    use super::super::{UrlFilter, UrlFilterError, MissingQueryError};
    use url::{self, Url};

    #[test]
    fn good() {
        let filter = IContact::new();

        let url = Url::parse("http://click.icptrack.com/icpf/relay.php?r=83759\
739&msgid=47283&act=8SSS&c=3748204&destination=https%3A%2F%2Fgradschool.duke.e\
du%2Fstudent-life%2Fevents%2F").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://gradschool.duke.edu/student-life/events/");
    }

    #[test]
    fn not_applicable() {
        let filter = IContact::new();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap_err(),
                   UrlFilterError::MissingQuery(MissingQueryError::new("destination")));
    }

    #[test]
    fn missing_query() {
        let filter = IContact::new();

        let url = Url::parse("http://click.icptrack.com/icp/relay.php?r=837597\
39&msgid=47283&act=8SSS&c=3748204").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap_err(),
                   UrlFilterError::MissingQuery(MissingQueryError::new("destination")));
    }

    #[test]
    fn broken_url() {
        let filter = IContact::new();

        let url = Url::parse("http://click.icptrack.com/icp/relay.php?r=837597\
39&msgid=47283&act=8SSS&c=3748204&destination=https%3A%2F%2Fgradschool.duke.ed\
u%3A65536%2Fstudent-life%2Fevents%2F").unwrap();
        assert!(filter.check(&url));
        match filter.apply(&url) {
            Err(UrlFilterError::UrlParse(url::ParseError::InvalidPort)) => assert!(true),
            _ => assert!(false),
        }
    }
}
