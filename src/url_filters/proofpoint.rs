// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! A filter to remove Proofpoint tracking.
//!
//! [Proofpoint TAP][] tracking is performed by modifying the URL to point to
//! Proofpoint's servers and including the (mangled) destination URL in the
//! query string. This filter extracts the true destination URL from the query
//! string.
//!
//! [Proofpoint TAP]: https://www.proofpoint.com/us/solutions/products/targeted-attack-protection
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::proofpoint::Proofpoint;
//!     use distrackt::url_filters::UrlFilter;
//!     use url::Url;
//!
//!     let example = "https://urldefense.proofpoint.com/v2/url?u=https-3A__www.example.com_&d=DjYWNg&c=imBPVzF25OoXeMqUInekcEgHoG1i6YHLR0Sj_gZ4adc&r=9K1TJwRX9UT0oTBpaN0e3eMBNuw7wQRGapYgr3gfCco&m=Ls728NI7rcl9RsB88RONCOEKJvcnn_gFAGQONa4jAqY&s=SDgfc-OXQp7mrGXwjaO8SNGInbLihvmpwVJuOjyuF-E&e=";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = Proofpoint::new();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "https://www.example.com/");
//! }
//! ```

use url::{percent_encoding, Url};
use super::{util, UrlFilter, UrlFilterError};

/// A filter to remove Proofpoint tracking.
#[derive(Clone)]
pub struct Proofpoint;

impl Proofpoint {
    /// Constructs a new `Proofpoint`.
    pub fn new() -> Proofpoint {
        Proofpoint {}
    }
}

impl UrlFilter for Proofpoint {
    fn check(&self, original: &Url) -> bool {
        original.domain() == Some("urldefense.proofpoint.com")
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        let mangled = try!(util::query_pair_value(&original, "u"));
        let perc_encoded = mangled.replace('_', "/").replace('-', "%");
        let fixed = try!(percent_encoding::percent_decode(
            perc_encoded.as_bytes()).decode_utf8());
        Ok(try!(Url::parse(&fixed)))
    }
}

#[cfg(test)]
mod tests {
    use super::Proofpoint;
    use super::super::{UrlFilter, UrlFilterError, MissingQueryError};
    use url::{self, Url};

    #[test]
    fn good() {
        let filter = Proofpoint::new();

        let url = Url::parse("https://urldefense.proofpoint.com/v2/url?u=https\
-3A__www.google.com_&d=DjYWNg&c=imBPVzF25OoXeMqUInekcEgHoG1i6YHLR0Sj_gZ4adc&r=\
9K1TJwRX9UT0oTBpaN0e3eMBNuw7wQRGapYgr3gfCco&m=Ls728NI7rcl9RsB88RONCOEKJvcnn_gF\
AGQONa4jAqY&s=SDgfc-OXQp7mrGXwjaO8SNGInbLihvmpwVJuOjyuF-E&e=").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://www.google.com/");
    }

    #[test]
    fn not_applicable() {
        let filter = Proofpoint::new();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap_err(),
                   UrlFilterError::MissingQuery(MissingQueryError::new("u")));
    }

    #[test]
    fn missing_query() {
        let filter = Proofpoint::new();

        let url = Url::parse("https://urldefense.proofpoint.com/v2/url?d=DjYWN\
g&c=imBPVzF25OoXeMqUInekcEgHoG1i6YHLR0Sj_gZ4adc&r=9K1TJwRX9UT0oTBpaN0e3eMBNuw7\
wQRGapYgr3gfCco&m=Ls728NI7rcl9RsB88RONCOEKJvcnn_gFAGQONa4jAqY&s=SDgfc-OXQp7mrG\
XwjaO8SNGInbLihvmpwVJuOjyuF-E&e=").unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap_err(),
                   UrlFilterError::MissingQuery(MissingQueryError::new("u")));
    }

    #[test]
    fn broken_url() {
        let filter = Proofpoint::new();

        let url = Url::parse("https://urldefense.proofpoint.com/v2/url?u=https\
-3A__www.google.com-3A65536_&d=DjYWNg&c=imBPVzF25OoXeMqUInekcEgHoG1i6YHLR0Sj_g\
Z4adc&r=9K1TJwRX9UT0oTBpaN0e3eMBNuw7wQRGapYgr3gfCco&m=Ls728NI7rcl9RsB88RONCOEK\
Jvcnn_gFAGQONa4jAqY&s=SDgfc-OXQp7mrGXwjaO8SNGInbLihvmpwVJuOjyuF-E&e=").unwrap();
        assert!(filter.check(&url));
        match filter.apply(&url) {
            Err(UrlFilterError::UrlParse(url::ParseError::InvalidPort)) => assert!(true),
            _ => assert!(false),
        }
    }
}
