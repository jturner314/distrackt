// Copyright 2016 Jim Turner. See the LICENSE file at the top-level directory
// of this distribution.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT
// or http://opensource.org/licenses/MIT>, at your option. You may not use this
// file except in compliance with the License.

//! A filter to remove SubID tracking.
//!
//! SubID tracking is performed by adding query parameters with any of the keys
//! listed in `SUBID_KEYS`. This filter removes all query pairs with any of
//! those keys.
//!
//! # Examples
//!
//! ```
//! extern crate distrackt;
//! extern crate url;
//!
//! fn main() {
//!     use distrackt::url_filters::subid::SubId;
//!     use distrackt::url_filters::UrlFilter;
//!     use url::Url;
//!
//!     let example = "http://www.example.com/?subid=f82abc7394d263eca845ee733ecf594a";
//!     let url = Url::parse(example).unwrap();
//!
//!     let filter = SubId::new();
//!     assert!(filter.check(&url));
//!     assert_eq!(filter.apply(&url).unwrap().as_str(), "http://www.example.com/?");
//! }
//! ```

use url::Url;
use super::{util, UrlFilter, UrlFilterError};

#[derive(Clone)]
/// A filter to remove SubID tracking.
pub struct SubId;

/// The query keys used for SubID tracking.
pub const SUBID_KEYS: &'static [&'static str] = &["aff_sub", "afftrack", "tid", "sid", "s1", "s2",
                                                  "s3", "s4", "s5", "u1", "u2", "u3", "u4", "u5",
                                                  "c1", "c2", "c3", "c4", "c5", "subid"];

impl SubId {
    /// Constructs a new `SubId`.
    pub fn new() -> SubId {
        SubId {}
    }
}

impl UrlFilter for SubId {
    fn check(&self, original: &Url) -> bool {
        original.query_pairs().any(|(key, _)| SUBID_KEYS.contains(&&(*key)))
    }

    fn apply(&self, original: &Url) -> Result<Url, UrlFilterError> {
        Ok(util::query_pairs_remove(&original, SUBID_KEYS))
    }
}

#[cfg(test)]
mod tests {
    use super::SubId;
    use super::super::UrlFilter;
    use url::Url;

    #[test]
    fn good() {
        let filter = SubId::new();

        let url = Url::parse("http://www.mouser.com/?utm_medium=email&utm_camp\
aign=elq-73.7442-eit-iss-brand-en&utm_source=eloqua&subid=f82abc7394d263eca845\
ee733ecf594a&utm_content=7639463&elqTrackId=0123456789abcdef73c2e1907d61ec3b&e\
lq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&elqat=1&elqCampaignId=6538")
            .unwrap();
        assert!(filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "http://www.mouser.com/?utm_medium=email&utm_campaign=elq-7\
3.7442-eit-iss-brand-en&utm_source=eloqua&utm_content=7639463&elqTrackId=01234\
56789abcdef73c2e1907d61ec3b&elq=f82abc7394d263eca845ee733ecf594a&elqaid=7352&e\
lqat=1&elqCampaignId=6538");
    }

    #[test]
    fn not_applicable() {
        let filter = SubId::new();

        let url = Url::parse("https://www.google.com/").unwrap();
        assert!(!filter.check(&url));
        assert_eq!(filter.apply(&url).unwrap().as_str(),
                   "https://www.google.com/?");
    }
}
