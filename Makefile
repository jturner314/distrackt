DISTRACKT = target/release/distrackt
MAN_PAGES = $(wildcard man/*.1)
MAN_PAGES_GZ = $(patsubst %,%.gz,$(MAN_PAGES))

.PHONY: default install

default: $(DISTRACKT) $(MAN_PAGES_GZ)

$(DISTRACKT):
	cargo build --release

man/%.gz: man/%
	gzip -k -f "$<"

install:
	install -d "$(DESTDIR)/usr/bin/"
	install -m 755 "$(DISTRACKT)" -t "$(DESTDIR)/usr/bin/"
#
	install -d "$(DESTDIR)/usr/share/man/man1/"
	install -m 644 man/*.1.gz -t "$(DESTDIR)/usr/share/man/man1/"
#
	install -d "$(DESTDIR)/usr/share/doc/distrackt/"
	install -m 644 README.md -t "$(DESTDIR)/usr/share/doc/distrackt/"

clean:
	git clean -fXdq
